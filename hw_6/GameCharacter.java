public abstract class GameCharacter{
	protected String name;
	protected int health;
	protected int ap;
	protected int xp;

public GameCharacter(String name, int health, int ap, int xp){	
		this.name=name;
		this.health=health;
		this.ap=ap;
		this.xp=xp;
	}

	public void takeDamage(int damage){
		this.health-=damage;
	}
	
	public void attack(GameCharacter opponent){
		opponent.takeDamage(this.getAp());
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getAp(){
		return this.ap;
	}
	
	public int getHealth(){
		return this.health;
	}
	
	public boolean status(){   
        boolean alive = true;
        if(!(this.health > 0)){       
            alive = false;
        }
        return alive;
    }
    public int getXp(){
 		return this.xp;
    	}

	public String getState(){
		String state = "Name: "+ getName()+"\n"
		+ "Atk: "+ getAp()+"\n"
	    + "HP: "+ getHealth()+"\n";
		return state;
	}
}