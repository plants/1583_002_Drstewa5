import java.util.Scanner;
import java.util.Random;

public class MyHouse{
static Player you = new Player("XXXppraiserXXX", 100, 30, 0, 0);
static boolean gameState =true;	
	public static void main(String[] args) {
		House antiquesRoadshow = new House();
		Room initialRoom = House.getRoom();
		
		gameLoop(you, initialRoom);
		System.out.println("APPRAISE THE ROOF!");
	}
	
	public static void gameLoop(Player you, Room currentRoom){
		Random numGen = new Random();
		
		Scanner scanner =new Scanner(System.in);
		System.out.println("Use (n) (e) (s) (w) to navigate the Roadshow, Bro! APPRAISE TILL YOU DIE!! (watch out for the APPLIANCES!)");

		
			while (gameState){	
				
				if(you.getXp()!= 0 && you.getXp()/ 2 ==0){
					System.out.println("UUUUUUUUUUHHHHHpgrade?!");
					you.upgrade();
					you.upgradeCounter++;
				}
	     		String line = "*********************************";
	     		String nothing = "Nothing to appraise that way...";
				String invalid = "Try looking for extreme appraisals!";
				System.out.println(line+"\n"+currentRoom);
				char path = scanner.nextLine().charAt(0);
				System.out.println("FANNY PACK (y)?");
				int chance = numGen.nextInt(5);
					
					if (chance==0){
						Monster opponent = monsterGenerator(numGen.nextInt(3));
						battle(you, opponent);
						if(!you.status()){
							System.out.println("YOUVE BEEN APPRAISED SUCKAHH");
							break;
						}
						else{
							getItem(you);
							you.xpUp(opponent.getXp());
							
							if(you.upgradeCounter==10){
							fridge();							
							}
						}
					}//end if 0	
									
				if (path == 'q'){
					gameState=false;
				}

				else if (path == 'y'){
					if(you.apUP>0 || you.hpUP>0){
	           		you.openBackpack();
	           		}
	           		else{
	        		System.out.println("Your FANNYPACK is empty, FOOL.");	
	        		}
				}
					
				else if (path == 'n'){
					if (currentRoom.getNorth() == null){
	                   System.out.println(nothing);
	            	}
	            	else{
	               	currentRoom=currentRoom.getNorth();
	                }
				}

				else if (path == 'e'){
			      	if (currentRoom.getEast() == null){
	                   System.out.println(nothing);
	                }
	                else{
	               	currentRoom=currentRoom.getEast();
	                }
				}
				
				else if (path == 's'){
					if (currentRoom.getSouth() == null){
	                   System.out.println(nothing);
	            	}
	            	else{
	               	currentRoom=currentRoom.getSouth();
	                }
				}

				else if (path == 'w'){
			      	if (currentRoom.getWest() == null){
	                   System.out.println(nothing);
	                }
	                else{
	               	currentRoom=currentRoom.getWest();
	                }
				}

				else{
					System.out.println(invalid);
				}
				
			}//end while			
			
	}//end gameloop	
	
	public static Monster monsterGenerator(int monsterType){
		Monster randomMonster= null;

		if(monsterType==0){
			randomMonster=new Monster("Unhinged Coffee table", 40, 30, 1);
		}

		if(monsterType==1){
			randomMonster=new Monster("Unforgivable Oriental rug", 50, 40, 2);
		}

		if(monsterType==2){
			randomMonster=new Monster("Besmirched Armoir", 70, 60, 5);
		}

		return randomMonster;
	}//end monster generator

	public static void battle(Player you, Monster opponent){
		System.out.println("You are appraising a(n) "+ opponent.getName()+"!");

		while( you.status() && opponent.status() ){
			//System.out.println(you.getState());
			//System.out.println(opponent.getState());
			you.takeTurn(opponent);
			opponent.takeTurn(you);	

		}//end while	
	}//end method battle
	
	public static void getItem(Player you){
	   Random numGen = new Random();
	   int item = numGen.nextInt(2);
	  
	   if(item==0){
		   you.addHpUP();
		   System.out.println("whoa cool, a picture of a cute dog. I'll save it for when I need it most.");
	   }

	   else{
		   you.addApUP();
		   System.out.println("This.java is magical. Better sip it slowwwwwwwwwwwwwwwwwwwwwwly.");  	   
	   }
   }//end getitem	

    public static void fridge(){
   	Monster fridge = new Monster("GODFORSAKEN REFRIDGERATOR", 150, 65,	1000);
   	battle(you,fridge);
   		
   		if(!you.status()){
			System.out.println("YOUVE BEEN APPRAISED SUCKAHH");
			System.out.println("ROADSHOW, OUTT!");
		gameState=false;
  	    }	
  	}    
}//end class
