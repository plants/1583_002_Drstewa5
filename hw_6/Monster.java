import java.util.Random;

public class Monster extends GameCharacter{

    private Random selector = new Random();
       
    public Monster(String name, int health, int ap, int xp){
        super(name, health, ap, xp);
        super.ap = ap + selector.nextInt(6) + 1;
    }

    public void takeTurn(Player opponent){
        attack(opponent);
    }
}