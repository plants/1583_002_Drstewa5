import java.util.Scanner;

public class Player extends GameCharacter{
	
	public int upgradeCounter=0;
	public int hpUP;
	public int apUP;
	private String choice;
	private int magic;
	private Scanner action = new Scanner(System.in);
	
	public Player(String name, int health,int ap,int xp,int magic){
		
		super(name,health,ap,xp);		
		this.magic=magic;
	}
 
	
    public void takeTurn(Monster opponent){
		combat(opponent);
	}
	
	public void combat(GameCharacter opponent){
		System.out.println(opponent.getState()+"\n");
		System.out.println(getState());
		selection();
		choice = action.next();
		checkChoice(choice, opponent);
	}
	
	public void selection(){
		System.out.println("1) Attack");
        System.out.println("2) Cast Spell");
        System.out.println("3) Inventory");
		System.out.println("4) Charge Mana");
	}
	
	public void checkChoice(String choice,GameCharacter opponent){
        if(choice.equals("1")){
        	this.attack(opponent);
        } 
        else if(choice.equals("2")){
            spell(opponent);
        } 
        else if(choice.equals("3")){
            
            if(apUP>0 || hpUP>0){
           		openBackpack();
        	}else{
        		System.out.println("Your FANNYPACK is empty, FOOL.");
        	}
        } 
        else if(choice.equals("4")){
			magic +=1;
		} 
        else {
            System.out.println("Not a Choice.");
 	    }
	}
    	
	public void upgrade(){
		this.health+=30;
		this.ap+=10;
	}
    	
   	public void xpUp(int exp){
   		this.xp+=exp;
    }
    	
   	public void levelUP(Monster opponent){
   		this.xp+=opponent.getXp();
   	}
    
    public void spendMagic(){
		this.magic=this.magic-3;
	}
			   
	public int getMagic(){
		return this.magic;
	}
	
	public void charge(){
		this.magic++;
	}
	
	public void spell(GameCharacter opponent){
		if (this.magic >=3){
			magicEffect();
			spendMagic();
		}

		else{
			System.out.println("Charge Magic");
		}
	}
	public void magicEffect(){
		this.ap=ap*2;
	}
	
	public void openBackpack(){
		boolean backpack = true;
		while(backpack==true){
		 	System.out.println("You have " + hpUP + " picture(s) of (a) dog(s) and " + apUP + " cup(s) of coffee.");
            System.out.println("1) Look at a picture of a dog.");
            System.out.println("2) Drink a cup of coffee.");
            System.out.println("3) Close backpack.");
            String item = action.next();
            
            if(item.equals("1")){ 
                useHpUP();
            } 
            else if(item.equals("2")){
                useApUP();
            } 
            else if(item.equals("3")){
                backpack = false;
            } 
            else {
                System.out.println("Choice not recognized.");
            }       	
        }
	}//end method
	
	public void addHpUP(){
		this.hpUP++;
	}
	
	public void useHpUP(){
        if(hpUP > 0){
            this.hpUP--;
            super.health += 20;
            System.out.println("You looked at a picture of a dog and felt a bit better. (+20HP)");
        } 

        else {
            System.out.println("I sure do wish i had a picture of a dog right now.");
        }
    }
	
	public void addApUP(){
		this.apUP++;
	}
	
	public void useApUP(){
        if(apUP > 0){
            this.apUP--;
            super.ap += 20;
            System.out.println("This truly is a magic brew. (+15AP)");
        } 

        else {
            System.out.println("Must've drank it all...");
        }
    }
    
    @Override
    public String getState(){
        String state = "Your"+super.getState();
        state += "Magic: " + getMagic() + "\n"
        + "XP: " + getXp();
        return state;
    }
}






