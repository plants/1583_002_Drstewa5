//class House
public class House{
	private static Room livingRoom;
   private static Room kitchen;
   private static Room diningRoom;
	private static Room bedroom;
	private static Room bathroom;
	private static Room frontPorch;
	private static Room office;
	private static Room backPorch;
 
   
    public House(){    
      frontPorch = new Room("Entrance to the roadshow. Time to get down to the GNARLY GRITTY");      
      livingRoom = new Room("Foyer: WHOA DAMN THATS A LOT OF TEAK!");
      bathroom = new Room("Backstage: Every dude back here looks like James Lipton, and ITS F****** AWESOME!");
      office = new Room("On Set: The value of this text-based video game is virtually PRICELESS! SUCK IT AMERICA!!");
      kitchen = new Room("Kitchenette: IT'S LIKE A KITCHEN BUT WITH MORE 'ETTE'! RADDDDD!!!!");
      diningRoom = new Room("Green Room: I'M ABOUT TO APPRAISE THIS VEGGIE PLATE MY DUDE! MANY THOUSANDS OF DOLLARS!");
      bedroom = new Room("Loading Area: BRING ON THE ANTIQUES, FROM ALL CORNERS OF THIS SICK ASS COUNTRY!");
      backPorch = new Room("Back Door: WIDDLY WIDDDLY WHAAAAAOOONGGGGG WAM!! WAM!!.");
      
      bedroom.setExits(null,diningRoom,office,null);
      diningRoom.setExits(backPorch,null,kitchen,bedroom);
      kitchen.setExits(diningRoom,null,livingRoom,office);
      office.setExits(bedroom,kitchen,bathroom,null);   
      bathroom.setExits(office,livingRoom,null,null);
      livingRoom.setExits(kitchen,null,frontPorch,bathroom);     
      frontPorch.setExits(livingRoom,null,null,null);
      backPorch.setExits(null,null,diningRoom,null);
      }
    
   public static Room getRoom(){
      return frontPorch;
	}
   
}
